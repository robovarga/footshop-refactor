<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

abstract class AbstractController
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig, Request $request)
    {
        $this->twig = $twig;
        $this->request = $request;
    }

    public function render()
    {
        $template = $this->getName() . ".html";
        $data = array_merge(
            [
                'type' => $this->getName(),
            ],
            $this->getData()
        );

        echo $this->twig->render($template, $data);
    }

    protected function getName()
    {
        $className = (new \ReflectionClass($this))->getShortName();
        $name = strtolower(preg_replace('/([a-zA-Z]*)Controller/i', '${1}', $className));

        return $name;
    }

    abstract protected function getData();
}

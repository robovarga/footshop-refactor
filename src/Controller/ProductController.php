<?php

namespace App\Controller;

use App\Log;
use App\Model\Brand;
use App\Model\Product;

class ProductController extends AbstractController
{

    protected function getData()
    {
        $parameters = $this->request->query->all();
        $name = (empty($parameters['name'])) ? null : $parameters['name'];
        $brand = (empty($parameters['brand'])) ? null : $parameters['brand'];
        $order = (empty($parameters['order'])) ? 'id' : $parameters['order'];
        $limit = (empty($parameters['limit'])) ? 10 : $parameters['limit'];

        Log::info(sprintf('Rendering products action.'), $parameters);

        $brandModel = new Brand();
        $brands = $brandModel->load();

        $productModel = new Product();
        $products = $productModel->load($name, $brand, $order, $limit);

        foreach ($products as $key => $product) {
            $products[$key]['sum_price'] = $product['price'] * $product['quantity'];
            $products[$key]['sum_reserved_price'] = $product['price'] * $product['reserved'];
        }

        return [
            'title' => 'Products',
            'products' => $products,
            'brands' => $brands,
        ];
    }
}

<?php

namespace App\Model;

use App\DB;

abstract class Model
{
    /**
     * @var DB
     */
    protected $db;

    public function __construct()
    {
        if (null === $this->db) {
            $this->db = DB::getInstance();
        }

        return $this->db;
    }

    protected function fetch($sql)
    {
        $stmt = $this->db->query($sql);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

}

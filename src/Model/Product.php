<?php

namespace App\Model;

class Product extends Model
{
    public function load($name = null, $brandID = null, $order = 'id', $limit = 10)
    {
        $where = [];
        if (!empty($name)) {
            $where[] = ["p.name LIKE %~like~", $name];
        }

        if (!is_null($brandID) && $brandID > 0) {
            $where[] = ["b.id = %iN", $brandID];
        }

        $result = $this->db->query(
            'SELECT p.*, b.name AS brand FROM products p JOIN brands b on p.brand_id = b.id',
            '%if', !empty($where), 'WHERE %and', $where, '%end',
            'ORDER BY %n ', $order, 'ASC',
            '%lmt', $limit
        );

        return $result->fetchAll();
    }
}

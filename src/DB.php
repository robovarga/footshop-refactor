<?php

namespace App;

final class DB
{
    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            $conn = new \Dibi\Connection([
                'driver' => 'PDO',
                'dsn' => "mysql:host=" . _DB_SERVER_ . ';port=3306;dbname=' . _DB_NAME_,
                'username' => _DB_USER_,
                'password' => _DB_PASSWD_,
            ]);

            self::$instance = $conn;
        }

        return self::$instance;
    }
}

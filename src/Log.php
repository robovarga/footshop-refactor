<?php

namespace App;

use Monolog\Handler\NullHandler;
use Monolog\Logger;

class Log
{
    public static $instance;

    public function __construct()
    {
        if (null == self::$instance) {
            $logger = new Logger('app');
            $logger->pushHandler(new NullHandler());

            self::$instance = $logger;
        }

        return self::$instance;
    }

    public static function debug($message, array $context = [])
    {
        self::$instance->addDebug($message, $context);
    }

    public static function info($message, array $context = [])
    {
        self::$instance->addInfo($message, $context);
    }

    public static function notice($message, array $context = [])
    {
        self::$instance->addNotice($message, $context);
    }

    public static function warning($message, array $context = [])
    {
        self::$instance->addWarning($message, $context);
    }

    public static function error($message, array $context = [])
    {
        self::$instance->addError($message, $context);
    }

    public static function critical($message, array $context = [])
    {
        self::$instance->addCritical($message, $context);
    }

    public static function alert($message, array $context = [])
    {
        self::$instance->addAlert($message, $context);
    }

    public static function emergency($message, array $context = [])
    {
        self::$instance->addEmergency($message, $context);
    }
}

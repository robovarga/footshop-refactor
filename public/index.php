<?php

use App\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/../config/config.php';
require __DIR__ . '/../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/../templates');
$twig = new Twig_Environment($loader);

// create a log channel
$log = new Logger('App');
$log->pushHandler(new StreamHandler(__DIR__ . '/../var/logs/main.log', Logger::DEBUG));
Log::$instance = $log;

try {
    $request = Request::createFromGlobals();

    switch ($request->query->get('type', 'product')) {
        case 'stats':
            $controller = new \App\Controller\StatsController($twig, $request);
            break;
        default:
            $controller = new \App\Controller\ProductController($twig, $request);
    }

    $controller->render();
} catch (\Throwable $e) {
    Log::critical($e->getMessage());
    echo $twig->render('error.html');
}
